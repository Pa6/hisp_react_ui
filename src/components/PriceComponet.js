import React, { useState, useEffect } from 'react'
import PriceService from '../services/PriceService';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';

  import { Bar } from 'react-chartjs-2';

  
// Price components
function PriceComponent() {


  
    const [prices, setPrices] = useState([])

    useEffect(() => {
    getPrices()
  
  }, [])
  
    
    const getPrices = () => {
        PriceService.getPrices().then((response) => {
            setPrices(response.data)
            console.log(response.data);
        }); 
    };



    return (
        
    <div className="container">

        <h1 className="text-center"  style={{backgroundColor: "DodgerBlue"}}> Bitcoin Historical Quote  </h1>

        <table className="table table-striped" >
             <thead style={{backgroundColor: "lightblue"}}>
                <tr>
                    <th>
                        Price
                    </th>
                    <th>
                        Volume
                    </th>
                    <th>
                        Market 
                    </th>
                    <th>
                       Circulating Supply
                    </th>
                    <th>
                       Total Supply
                    </th>
                    <th>
                       Time
                    </th>
                </tr>
                </thead>
            <tbody>
                {
                    prices.map(
                        price=>
                        <tr key = {price.timestamp}>
                            <td>{price['quote']['USD'].price}</td>
                                <td> {price['quote']['USD'].volume_24h}</td>
                                <td> {price['quote']['USD'].market_cap}</td>
                                <td>   {price['quote']['USD'].circulating_supply}</td>
                                <td>   {price['quote']['USD'].total_supply}</td>
                                <td>   {price['quote']['USD'].timestamp}</td>

                        </tr> 

                    )
                }
            </tbody>

            <thead style={{backgroundColor: "lightblue"}}>
                <tr>
                    <th>
                        Price
                    </th>
                    <th>
                        Volume
                    </th>
                    <th>
                        Market 
                    </th>
                    <th>
                       Circulating Supply
                    </th>
                    <th>
                       Total Supply
                    </th>
                    <th>
                       Time
                    </th>
                </tr>
                </thead>
            

        </table>
    </div>
  )
}

export default PriceComponent