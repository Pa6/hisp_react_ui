import axios from 'axios';


//API URL
const PRICE_REST_API_URL = 'http://localhost:8000/api/get-price';

class PriceService{

    // call price historical api
    getPrices(){
        return axios.get(PRICE_REST_API_URL);
    }
}

export default new PriceService();   
